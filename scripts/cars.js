window.onload = function() {
  console.log(sessionStorage.getItem('logged'));
  if(
    sessionStorage.getItem('logged')=='true'){
    userIsLoggedIn(sessionStorage.getItem('username'));
  }
  else{
    userIsNotLoggedIn();
  }
  search();
}

function logout(){
  sessionStorage.setItem('logged','false');
  window.location.href = "index.html";
}

function userIsLoggedIn(username){
  //Add Edit My Profile to navbar
  var liEdit = document.createElement("li");
  liEdit.setAttribute("class","nav-item");
  liEdit.setAttribute("id","li-edit");
  document.getElementById("ul-nav").appendChild(liEdit);

  var aEdit = document.createElement("a");
  aEdit.setAttribute("class","nav-link");
  aEdit.setAttribute("href","edit.html");
  aEdit.setAttribute("id","edit");
  aEdit.innerText="Edit My Profile";
  document.getElementById("li-edit").appendChild(aEdit);

  //Logged as x , na navbar
  var ul = document.createElement("ul");
  ul.setAttribute("class","nav navbar-nav ml-auto");
  ul.setAttribute("id","logged-as");
  document.getElementById("nav").appendChild(ul);

  var li = document.createElement("li");
  li.setAttribute("class","nav-item");
  li.setAttribute("style","color: white; margin-top: 7px;");
  li.setAttribute("id","li-logged-as");
  li.innerText = "Logged as: "+username + " | ";
  document.getElementById("logged-as").appendChild(li);


  //Logout - li
  var li = document.createElement("li");
  li.setAttribute("class","nav-item");
  li.setAttribute("id","li-logged");
  document.getElementById("logged-as").appendChild(li);

  //Logout - link
  var link = document.createElement("a");
  link.setAttribute("class","nav-link");
  link.setAttribute("onclick","logout()");
  link.setAttribute("href","");
  link.setAttribute("id","a-logged");
  document.getElementById("li-logged").appendChild(link);

  //Logout - span
  var span = document.createElement("span");
  span.setAttribute("class","fas fa-user");
  document.getElementById("a-logged").appendChild(span);
  link.innerText = "Logout";

}

function userIsNotLoggedIn(){
  //Gerar os botoes de sign up e de login
  //Sign up - ul
  var ul = document.createElement("ul");
  ul.setAttribute("class","nav navbar-nav ml-auto");
  ul.setAttribute("id","not-logged");
  document.getElementById("nav").appendChild(ul);

  //Sign up - li
  var li = document.createElement("li");
  li.setAttribute("class","nav-item");
  li.setAttribute("id","li-not-logged");
  document.getElementById("not-logged").appendChild(li);

  //Sign up - link
  var link = document.createElement("a");
  link.setAttribute("class","nav-link");
  link.setAttribute("href","signUp.html");
  link.setAttribute("id","a-not-logged");
  document.getElementById("li-not-logged").appendChild(link);

  //Sign up - span
  var span = document.createElement("span");
  span.setAttribute("class","fas fa-user");
  document.getElementById("a-not-logged").appendChild(span);
  link.innerText = "Sign up";

  //Login - li
  var li = document.createElement("li");
  li.setAttribute("class","nav-item");
  li.setAttribute("id","login-li-not-logged");
  document.getElementById("not-logged").appendChild(li);

  //Login - link
  var link = document.createElement("a");
  link.setAttribute("class","nav-link");
  link.setAttribute("href","login.html");
  link.setAttribute("id","login-a-not-logged");
  document.getElementById("login-li-not-logged").appendChild(link);

  //Login- span
  var span = document.createElement("span");
  span.setAttribute("class","fas fa-user");
  document.getElementById("login-a-not-logged").appendChild(span);
  link.innerText = "Login";

}


/* Processamento dos resultados - API */

/**
  * Função que faz chamadas à API
  *
  * @param {String} url URL para fazer chamada à API
  * @return {String} json_parsed Parsed Json
*/
async function APIJson(url) {
  let json = await fetch(url)
    .then((response) => response.text())
    .then(data => {
      return data;
    })
    .catch(error => {
      console.error(error);
      return null;
    });
  let json_parsed = JSON.parse(json);
  return json_parsed;
}

/**
  * Função que determina os valores do drop down das Brands
  *
*/
async function dropBrand(){
  var brandDrop = document.getElementById("dropBrand"); //Dropdown das Brands
  const brandBtn = document.getElementById("btBrand");  //Butão corresponde aos Models

  //Preparar o dropdown das Brands
  while(brandDrop.firstChild){
    brandDrop.removeChild(brandDrop.firstChild);
  }

  //Obter JSON das Brands
  const json_parsed = await APIJson("http://192.168.160.72:8080/carmarketplace/carBrands");

  buildDrop(brandDrop.id, brandBtn.id, "AllBrands", "AllBrands", "All Brands");

  for(var brand of json_parsed) {
    buildDrop(brandDrop.id, brandBtn.id, brand.brandID, brand.brand, brand.brand);
  }
}

/**
  * Função que determina os valores do drop down dos Models
  *
*/
async function dropModel() {
  const brandChosen = document.getElementById("btBrand"); //Valor selecionado da Brand
  const modelDrop = document.getElementById("dropModel"); //Dropdown dos Models
  const modelBtn = document.getElementById("btModel");  //Butão corresponde aos Models


  //Obter JSON dos Modelos
  const url = "http://192.168.160.72:8080/carmarketplace/models/brand=" + brandChosen.getAttribute('value');
  const json_parsed = await APIJson(url);

  //Preparar o dropdown do modelo
  while(modelDrop.firstChild){
    modelDrop.removeChild(modelDrop.firstChild);
  }

  buildDrop(modelDrop.id, modelBtn.id, "AllModels", "AllModels", "All Models");

  //Caso não seja escolhido o All Brands
  if (brandChosen.innerText != "All Brands") {
    for(let model of json_parsed) {
      buildDrop(modelDrop.id, modelBtn.id, model, model, model);
    }
  }
}

/**
  * Função de assistência à construção dos dropdowns adicionando informação desejada
  *
  * @param {String} dropdownID  Id do dropdown a ser utilizado
  * @param {String} button  Id do butão a ser alterado
  * @param {String} id  Id da novo elemento <li>
  * @param {String} value Value da novo elemento <li>
  * @param {String} text  Text da novo elemento <li>
  *@return {Number} 0 caso o dropdownID não exista
*/
function buildDrop(dropdownID, button, new_id, new_value, new_text) {
  if (document.getElementById(dropdownID)) {
    var dropdown = document.getElementById(dropdownID);
  }
  else {
    return 0;
  }

  //Novo li
  let new_li = document.createElement("li");
  new_li.classList.add("dropdown-item");
  new_li.setAttribute("id", new_id);
  new_li.setAttribute("value", new_value);
  new_li.innerText = new_text;

  //Adicionar função de click ao novo li
  new_li.addEventListener("click", function(){
    let button_change = document.getElementById(button);
    button_change.innerText = new_text;
    button_change.value = new_id;
   });

  //Adicionar novo elemento li ao dropdown
  dropdown.appendChild(new_li);
}

/**
  * Função de pesquisa de resultados
  *
*/
async function search(){

  //Obter valores dos parâmetros de pesquisa
  var brandChosen = document.getElementById("btBrand");
  var modelChosen = document.getElementById("btModel");
  var maxPrice = document.getElementById("maxPrice");

  //Preparar div dos resultados
  var results_div = document.getElementById("search_results");
  while(results_div.firstChild){
    results_div.removeChild(results_div.firstChild);
  }

  //Chamada à API
  var url;
  if ((brandChosen.innerText.trim() == "Brand" && modelChosen.innerText.trim() == "Model") ||
      (brandChosen.innerText == "All Brands" && modelChosen.innerText == "All Models") ||
      (brandChosen.innerText == "All Brands" && modelChosen.innerText.trim() == "Model")) {
        url = "http://192.168.160.72:8080/carmarketplace/ads/type=Car/brand=/model=/fuel=/price=" + maxPrice.value;
  }
  else if ((brandChosen.innerText != "All Brands" && modelChosen.innerText == "All Models")  ||
            (brandChosen.innerText != "All Brands" && modelChosen.innerText.trim() == "Model")) {
    url = "http://192.168.160.72:8080/carmarketplace/ads/type=Car/brand=" + brandChosen.innerText + "/model=/fuel=/price=" + maxPrice.value;
  }
  else if (brandChosen.innerText != "All Brands" && modelChosen.innerText != "All Models") {
    url = "http://192.168.160.72:8080/carmarketplace/ads/type=Car/brand=" + brandChosen.innerText + "/model=" + modelChosen.innerText + "/fuel=/price=" + maxPrice.value;
  }

  var json_parsed = await APIJson(url);

  //Verificar se json_parsed retorna array com 0 elementos
  if(json_parsed.length != 0) {
    //Iterar sobre os Anúncios
    for (var anuncio of json_parsed) {
      buildAd(anuncio);
    }
  }
  else {
    //Não há resultados
    var results_div = document.getElementById("search_results");
    var result = document.createElement("h1");
    result.classList.add("top-margin", "text-center", "col-xl-12");
    result.innerText = "No results!";
    results_div.appendChild(result);
  }
}

/**
  * Função de assistência à construção dos dropdowns adicionando informação desejada
  *
  * @param {String} ad  Anúncio a ser colocado nos resultados
*/
function buildAd(ad) {
  //Div dos resultados
  var results_div = document.getElementById("search_results");

  //Div do anúncio
  var result = document.createElement("div");
  result.classList.add("col-xl-3", "top-margin",  "justify-content-center", "text-center");

  //Verificar e adicionar imagem do anúncio ou default
  var adFigure = document.createElement("figure");
  adFigure.classList.add("figure");
  var imgCar = document.createElement("img");
  imgCar.setAttribute("class","figure-img img-fluid rounded");
  if (ad.imagem == null) {
    imgCar.src = "img/default-image.jpg";
  }
  else {
    imgCar.src = ad.imagem;
  }
  adFigure.appendChild(imgCar);

  //Adicionar informação da marca, modelo e versão
  var captionBrandModel = document.createElement("figcaption");
  captionBrandModel.classList.add("figure-caption");
  //Verificar se o carro tem versão
  if (ad.version != null) {
    captionBrandModel.innerText = ad.brand + " " + ad.model + " - " + ad.version;
  }
  else {
    captionBrandModel.innerText = ad.brand + " " + ad.model;
  }
  adFigure.appendChild(captionBrandModel);

  //Criar e adicionar span vermelho para o preço
  captionCar = document.createElement("figcaption");
  captionCar.classList.add("figure-caption");
  const spanPrice = document.createElement("span");
  spanPrice.style.color = "red"
  spanPrice.innerText = ad.price + " €";
  captionCar.appendChild(spanPrice);
  adFigure.appendChild(captionCar);

  //Adicionar localização
  captionCar = document.createElement("figcaption");
  captionCar.classList.add("figure-caption");
  captionCar.innerText = ad.location;
  adFigure.appendChild(captionCar);

  //Criar link que fique a envolver o anúncio
  const hyperlink = document.createElement("a");
  hyperlink.setAttribute("data-toggle", "modal");
  hyperlink.setAttribute("id","a-modal");
  hyperlink.setAttribute("data-target", "#adModal");
  hyperlink.addEventListener("click", function() {buildModal(ad.anuncioID);});
  hyperlink.appendChild(adFigure);

  //Juntar info do anúncio à div do mesmo
  result.appendChild(hyperlink);

  //Juntar à div dos resultados
  results_div.appendChild(result);
}

/**
  * Função de assistência à construção dos Modals
  *
  * @param {String} adID  Id do anúncio
*/
async function buildModal(adID) {
  var name = document.getElementById("sellerName");
  var url = "http://192.168.160.72:8080/carmarketplace/ad=" + adID;
  var json_parsed = await APIJson(url);

  //Alterar Imagem
  var img = document.getElementById("adModalImg");
  if (json_parsed.image != null) {
    img.src = json_parsed.image;
  }
  else {
    img.src = "img/default-image.jpg";
  }

  //Alterar Brand
  var brand = document.getElementById("adModalBrand");
  brand.innerText = json_parsed.brand;

  //Alterar Model
  var model = document.getElementById("adModalModel");
  model.innerText = json_parsed.model;

  //Alterar Versão
  var version = document.getElementById("adModalVersion");
  var versionTag = document.getElementById("versionTag");
  if (json_parsed.version == null) {
    version.innerText = "";
    versionTag.style.display = "none";
    version.style.display = "none";
  }
  else {
    version.innerText = json_parsed.version;
    versionTag.style.display = "block";
    version.style.display = "block";
  }

  //Aterar Preço
  var price = document.getElementById("adModalPrice");
  price.innerText = json_parsed.price;

  //Alterar Localização
  var location = document.getElementById("adModalLocation");
  location.innerText = json_parsed.location;

  //Aterar Fuel
  var fuel = document.getElementById("adModalFuel");
  fuel.innerText = json_parsed.fuel;

  //Alterar Cylinder
  var cylinder = document.getElementById("adModalCylinder");
  cylinder.innerText = json_parsed.cylinder;

  //Alterar Power
  var power = document.getElementById("adModalPower");
  power.innerText = json_parsed.power;

  //Alterar Color
  var color = document.getElementById("adModalColor");
  color.innerText = json_parsed.color;

  //Alterar Regist
  var regist = document.getElementById("adModalRegist");
  regist.innerText = json_parsed.month + "-" + json_parsed.year;

  //Alterar Note
  var note = document.getElementById("adModalAnnotation");
  var annotationTag = document.getElementById("annotationTag");
  if (json_parsed.annotation == null) {
    note.innerText = "";
    annotationTag.style.display = "none";
    note.style.display = "none";
  }
  else {
    note.innerText = json_parsed.annotation;
    annotationTag.style.display = "block";
    note.style.display = "block";
  }

  //Alterar Vendedor
  var seller = document.getElementById("adModalSeller");
  seller.innerText = json_parsed.userName;

  //Alterar Contacto
  var contacto = document.getElementById("adModalContact");
  contacto.innerText = json_parsed.contacto;

  //Alterar Email
  var email = document.getElementById("adModalEmail");
  email.innerText = json_parsed.email;



}

/* FIM Processamento dos resultados - API */

/* FUNÇÕES DO PRICE RANGE SLIDER */

/**
    * Função de update do input correspondente ao máximo
    * para o slider máximo
    *
  */
function sliderRightInput(){
  var sliderRight=document.getElementById("maxSlider");
  var inputMax=document.getElementById("maxPrice");
  sliderRight.value=(inputMax.value);//chnage in input max updated in slider right
}

/**
    * Função de update do slider correspondente ao máximo
    * para o input máximo
    *
  */
function inputMaxSliderRight(){
  var sliderRight=document.getElementById("maxSlider");
  var inputMax=document.getElementById("maxPrice");
  inputMax.value=sliderRight.value;
}

/* FIM FUNÇÕES DO PRICE RANGE SLIDER */
